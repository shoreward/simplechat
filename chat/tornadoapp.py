# -*- coding: utf-8 -*-
import datetime
import json
import hashlib
import logging

import brukva
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.httpclient

from django.conf import settings
from django.utils.importlib import import_module
from django.contrib.auth.models import User

session_engine = import_module(settings.SESSION_ENGINE)

c = brukva.Client()
c.connect()

SENDER = 'server'

class MessagesHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, *args, **kwargs):
        super(MessagesHandler, self).__init__(*args, **kwargs)
        self.client = brukva.Client()
        self.client.connect()

    def check_origin(self, origin):
        return True

    def open(self, chat_type, oid):
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)
        session = session_engine.SessionStore(session_key)
        try:
            self.user_id = session["_auth_user_id"]
            self.user = User.objects.get(id=self.user_id)
            self.sender_name = self.user.username
        except (KeyError, User.DoesNotExist):
            logging.error('User.DoesNotExist %s' % self.user_id)
            self.close()

        self.channel = self._make_key(chat_type, oid)
        self.client.subscribe(self.channel)
        self.client.listen(self.show_new_message)
        self._broadcast_new_user()

    def handle_request(self, response):
        pass

    def _make_key(self, chat_type, oid):
        if chat_type == 'room':
            return 'thread_{}{}'.format(chat_type, oid)
        if chat_type == 'user':
            ids = [int(oid), int(self.user.id)]
            return 'thread_{}-{}'.format(max(ids), min(ids))


    def _broadcast_new_user(self):
        self._publish(u'{} присоединился к нам!'.format(self.sender_name), sender=SENDER)

    def _broadcast_user_left(self):
        self._publish(u'{} больше не нравится этот чат...'.format(self.sender_name), sender=SENDER)

    def on_message(self, message):
        if not message:
            return
        if len(message) > 10000:
            return
        try:
            data = json.loads(message)
            assert data['hash'] == hashlib.md5(data['text'].encode('utf8')).hexdigest()
        except (ValueError, AssertionError):
            def resend():
                logging.error('request msg resend for {}'.format(self.sender_name))
                self.write_message(str(json.dumps({"command": 'resend'})))

            tornado.ioloop.IOLoop.instance().add_timeout(
                datetime.timedelta(0.00001),
                resend
            )
            return
        self._publish(data['text'])

    def show_new_message(self, result):
        self.write_message(str(result.body))

    def _publish(self, text, sender=None):
        c.publish(self.channel, json.dumps({
            "sender": sender or self.sender_name,
            "text": text,
            "hash": hashlib.md5(text.encode('utf8')).hexdigest(),
        }))

    def on_close(self):
        def check():
            if self.client.connection.in_progress:
                tornado.ioloop.IOLoop.instance().add_timeout(
                    datetime.timedelta(0.00001),
                    check
                )
            else:
                self.client.disconnect()
        try:
            self.client.unsubscribe(self.channel)
            self._broadcast_user_left()
        except AttributeError:
            pass
        tornado.ioloop.IOLoop.instance().add_timeout(
            datetime.timedelta(0.00001),
            check
        )

application = tornado.web.Application([
    (r'/ws/(?P<chat_type>room|user)/(?P<oid>\d+)/', MessagesHandler),
])
