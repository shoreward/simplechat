/**
 * Created by shoreward on 09.06.15.
 */
last_msg = '';
function activate_my_chat(type, id) {
    var ws;
    function start_chat_ws() {
        ws = new WebSocket("ws://chat.ru/ws/"+ type +"/" +id + "/");
        //ws = new WebSocket("ws://chat.ru/ws/");
        ws.onmessage = function(event) {
            console.log('Onmessage',event.data, event );
            var message_data = JSON.parse(event.data);

            if (message_data.command){
                if (message_data.command == 'resend'){
                    send_ws(last_msg);
                }
            }else{
                 if (message_data.hash == MD5(message_data.text) ) {
                     var text = message_data.sender + ": " + message_data.text;

                 }else{
                     var text = 'Извините, сообщение от '+ message_data.sender +' доставлено некорректно и не будет показано!';
                 }
                $("#conversation").html($("#conversation").html() + text + "<br>")
            }
        }

        ws.onclose = function(){
            setTimeout(function() {start_chat_ws()}, 5000);
        };
    }

    if ("WebSocket" in window) {
        start_chat_ws();
    } else {
        $("form.message_form").html('<div class="outdated_browser_message"><p><em>Ой!</em> Вы используете устаревший браузер. Пожалуйста, установите любой из современных:</p><ul><li>Для <em>Android</em>: <a href="http://www.mozilla.org/ru/mobile/">Firefox</a>, <a href="http://www.google.com/intl/en/chrome/browser/mobile/android.html">Google Chrome</a>, <a href="https://play.google.com/store/apps/details?id=com.opera.browser">Opera Mobile</a></li><li>Для <em>Linux</em>, <em>Mac OS X</em> и <em>Windows</em>: <a href="http://www.mozilla.org/ru/firefox/fx/">Firefox</a>, <a href="https://www.google.com/intl/ru/chrome/browser/">Google Chrome</a>, <a href="http://ru.opera.com/browser/download/">Opera</a></li></ul></div>');
        return false;
    }

    function send_message() {
        var textarea = $("textarea#message_textarea");
        if (textarea.val() == "") {
            return false;
        }
        if (ws.readyState != WebSocket.OPEN) {
            return false;
        }
        var text = textarea.val();
        last_msg = text
        send_ws(text)
        textarea.val("");
    }

    function send_ws(text){
        ws.send(JSON.stringify({
            'text': text,
            'hash': MD5(text),
        }));
    }

    $("form.message_form div.send button").click(send_message);

    $("textarea#message_textarea").keydown(function (e) {
        // Ctrl + Enter
        if (e.ctrlKey && e.keyCode == 13) {
            send_message();
        }
    });
}


