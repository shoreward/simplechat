from django.conf.urls import patterns, url, include

urlpatterns = patterns('',
    url(r'^$', 'chat.views.index', name='index'),
    url(r'^chat/(?P<chat_type>room|user)/(?P<oid>\d+)/$', 'chat.views.choice_chat_view', name="chat_view"),
)
