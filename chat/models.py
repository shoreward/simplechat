# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User


class Room(models.Model):
    name = models.CharField(max_length=25)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        verbose_name = u'комната'
        verbose_name_plural = u'комнаты'
