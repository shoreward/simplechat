# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from .models import Room

# from django.core.urlresolvers import reverse
# room_url = lambda x: reverse('chat_view', args=['room', str(x.id)])


class RoomsForm(forms.Form):
    rooms = forms.ChoiceField(label=u'комнатах', choices=[('', '')] + [(o.pk, o.name) for o in Room.objects.all()])


class UsersForm(forms.Form):
    users = forms.ChoiceField(label=u'пользователем', choices=[('', '')] + [(o.pk, o.username) for o in User.objects.all()])
