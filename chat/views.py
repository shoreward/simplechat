from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from chat.forms import UsersForm, RoomsForm
from .models import Room


@login_required
def index(request):
    return redirect('chat_view', chat_type='room', oid=1)

@login_required
def choice_chat_view(request, chat_type, oid):
    context = {
        'users': UsersForm(),
        'rooms': RoomsForm(),
        'id': oid,
        'chat_type': chat_type,
        'user': request.user
    }

    if chat_type == 'user':
        context['object'] = get_object_or_404(User, pk=oid)
    if chat_type == 'room':
        context['object'] = get_object_or_404(Room, pk=oid)

    return render(request, 'chat.html', context)



